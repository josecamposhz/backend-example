require('dotenv').config();

const app = require('./config/app');

async function main () { // async necesario para await
    await app.listen(app.get('port')); // Metodo asincrono
    console.log('Port', app.get('port')) // Muestra el puerto
}

main();
const Sequelize = require('sequelize')
const db = require('../../config/dbConnection')

module.exports = db.sequelize.define(
  'notas',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    titulo: {
      type: Sequelize.STRING
    },
    descripcion: {
      type: Sequelize.STRING
    }
  },
  {
    timestamps: true
  }
)
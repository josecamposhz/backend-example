const usersController = {};
const config = require('../../config/config');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../Models/User')

//Para encriptar password
const saltRounds = 10;
var salt = bcrypt.genSaltSync(saltRounds);


usersController.all = async (req, res) => {
    User.findAll({
        attributes: ['nombre', 'apellidos', 'email']
    })
    .then(users => {
        if (users) {
            return res.status(200).json({ data: users })
        } else {
            return res.status(404).send('Users Not Found.')
        }
    })
    .catch(err => {
         res.send('error: ' + err)
    })
}

usersController.create = async (req, res) => {
    User.create({
        nombre: req.body.nombre,
        apellidos: req.body.apellidos,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, salt) // Se encripta la contraseña
    }).then(user => {
        res.status(201).send("Usuario creado correctamente.");
    }).catch(err => {
        res.status(500).send("error: " + err);
    })
}

usersController.find = async (req, res) => {
    User.findOne({
        where: {
            id: req.params.id
        },
        attributes: ['nombre', 'ap_paterno', 'ap_materno', 'rut', 'email']
    }).then(user => {
        if (user) {
            return res.status(200).json({ data: user })
        } else {
            return res.status(404).send('Usuario no encontrado.')
        }
    }).catch(err => {
        res.send('error: ' + err)
    })
}
usersController.update = (req, res) => {
    User.update(
        {
            nombre: req.body.nombre, apellidos: req.body.apellidos, email: req.body.email
        },
        {
            where: {
                id: req.params.id
            }
        }
    ).then(user => {
        if (user) {
            return res.json("Usuario actualizado.")
        } else {
            return res.status(404).send('Usuario no encontrado.')
        }
    }).catch(err => {
        res.send('error: ' + err)
    });
}

usersController.delete = async (req, res) => {
    User.destroy({
        where: {
            id: req.params.id
        }
    }).then(user => {
        if (user) {
            return res.json("Usuario eliminado")
        } else {
            return res.status(404).send('Usuario no encontrado.')
        }
    }).catch(err => {
        res.send('error: ' + err)
    });
}

//metodo que permite logearse al sistema
usersController.loginUser = async (req, res) => {
    User.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if (!user) {
            return res.status(404).send({ error: 'El correo ingresado no se encuentra en nuestros registros.' });
        }
        // Verificamos si las contraseñas son iguales
        let passwordValid = bcrypt.compareSync(req.body.password, user.password)
        if (!passwordValid) {
            return res.status(401).send({ error: "Contraseña incorrecta." });
        }
        const payload = {
            id: user.id,
            email: user.email
        }
        var token = jwt.sign(payload, config.secret, {
            expiresIn: 86400 // expires in 24 hours
        });
        res.status(200).send({ user: user, token: token });
    }).catch(err => {
        res.status(500).send('error: ' + err);
    });
}

module.exports = usersController;
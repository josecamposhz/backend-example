const notaController = {};

const Nota = require('../Models/Nota')

notaController.all = async (req, res) => {
    Nota.findAll().then(notas => {
        if (notas) {
            return res.status(200).json({ data: notas })
        } else {
            return res.status(404).send('Error.')
        }
    }).catch(err => {
            res.send('error: ' + err)
    })
}

notaController.create = async (req, res) => {
    Nota.create({
        titulo: req.body.titulo,
        descripcion: req.body.descripcion,
        autor_id: req.body.autor_id
    }).then(nota => {
        return res.status(200).json({ data: nota })
    }).catch(err => {
        res.status(500).send("error: " + err);
    })
}

notaController.find = async (req, res) => {
    Nota.findOne({
        where: {
            id: req.params.id
        }
    }).then(nota => {
        if (nota) {
            return res.status(200).json({ data: nota })
        } else {
            return res.status(404).send('Nota no encontrada.')
        }
    }).catch(err => {
        res.send('error: ' + err)
    })
}
notaController.update = (req, res) => {
    Nota.update(
        {
            titulo: req.body.titulo,
            descripcion: req.body.descripcion,
            ap_materno: req.body.ap_materno,
            autor_id: req.body.autor_id
        },
        {
            where: {
                id: req.params.id
            }
        }
    ).then(nota => {
        if (nota) {
            return res.json("Nota actualizada.")
        } else {
            return res.status(404).send('Nota no encontrada.')
        }
    }).catch(err => {
        res.send('error: ' + err)
    });
}

notaController.delete = async (req, res) => {
    Nota.destroy({
        where: {
            id: req.params.id
        }
    }).then(nota => {
        if (nota) {
            return res.json("Nota eliminada.")
        } else {
            return res.status(404).send('Nota no encontrada.')
        }
    }).catch(err => {
        res.send('error: ' + err)
    });
}

module.exports = notaController;
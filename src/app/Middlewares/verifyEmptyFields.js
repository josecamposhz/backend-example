User = (req, res, next) => {
    // Se valida si el campo nombre esta vacio
    if (req.body.nombre === '') {
        return res.status(400).send({ error: 'El campo Nombre es requerido.' })
    } else if (req.body.apellidos === '') {
        return res.status(400).send({ error: 'El campo Apellidos es requerido.' })
    } else if (req.body.password === '') {
        return res.status(400).send({ error: 'El campo contraseña es requerido.' })
    } else if (req.body.password.length < 4) {
        return res.status(400).send({ error: 'La contraseña debe tener a lo menos 4 caracteres.' })
    } else {
        next();
    }
}

Nota = (req, res, next) => {
    // Se valida si el campo nombre esta vacio
    if (req.body.titulo === '') {
        return res.status(400).send({ error: 'El campo Titulo es requerido.' })
    } else if (req.body.descripcion === '') {
        return res.status(400).send({ error: 'El campo Descripción es requerido.' })
    } else {
        next();
    }
}

module.exports = {
    User, Nota
}
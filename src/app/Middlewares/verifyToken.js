const jwt = require('jsonwebtoken');
const config = require('../../config/config');

verifyToken = (req, res, next) => {
  let token = req.headers['authorization'];
  // Se verifica si el request posee el headers authorization
  if (!token) {
    return res.status(403).send({
      message: 'No token provided.'
    });
  }

  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(500).send({
        message: 'Error:' + err
      });
    }
    req.userId = decoded.id;
    next();
  });
}

module.exports = verifyToken;
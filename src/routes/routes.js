const { Router } = require('express');
const router = Router();
// Middlewares
const verifyToken = require('../app/Middlewares/verifyToken');
const verifyEmail = require('../app/Middlewares/verifyEmail');
const verifyEmptyFields = require('../app/Middlewares/verifyEmptyFields');

const users = require('../app/Controllers/UserController');
const notas = require('../app/Controllers/NotaController');

router.post('/login', users.loginUser);

router.get('/users', verifyToken, users.all);
router.post('/users', verifyEmail, verifyEmptyFields.User, users.create);
router.get('/users/:id', verifyToken, users.find);
router.put('/users/:id', verifyToken, users.update);
router.delete('/users/:id', verifyToken, users.delete);

router.get('/notas', verifyToken, notas.all);
router.post('/notas', verifyToken, verifyEmptyFields.Nota, notas.create);
router.get('/notas/:id', verifyToken, notas.find);
router.put('/notas/:id', verifyToken, notas.update);
router.delete('/notas/:id', verifyToken, notas.delete);

module.exports = router;
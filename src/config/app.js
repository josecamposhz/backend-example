const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.urlencoded({ extended: false })) // parse application/x-www-form-urlencoded
app.use(bodyParser.json()) // parse application/json

// Settings
app.set('port', process.env.DB_PORT || 2020);

// Middlewares
app.use(cors()); //Utilizado para habilitar solicitudes HTTP
app.unsubscribe(express.json());

// Routes
app.use('/api', require('../../src/routes/routes'))

module.exports = app;
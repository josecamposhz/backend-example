// Conexion a la DB utilizado sequelize
const Sequelize = require('sequelize')
const db = {}
// nombre de la tabla, nombre de usuario, contraseña, normalmente por default es root el usuario y una contraseña vacia
const sequelize = new Sequelize('backend', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
})

db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db